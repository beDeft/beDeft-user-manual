<meta charset="utf-8" emacsmode="-*- markdown -*-"><link rel="stylesheet" href="../apidoc.css?"">

**[beDeft](../index.md.html)/Fragment**

Binding to C++ FragmentDataReference object describing any reference to a particular fragment 
(i.e. Fragment Data object), through a spatial transform. FragmentData C++ objects are not 
accessible directly through Lua, only their reference through a spatial transform are. 

Several references can point toward the same instance of a fragment, allowing thus to mutualize
the Fragment properties for its different instances. Spatially dependant properties of a reference 
are thus transformed from the base fragment frame into the reference frame when requested.



************************************************
*      Lua          |            C++           *
*                   |                          *
*  .----------.     |                          *
* | instance 1 | *--T1-.                       *
*  '----------'     |   |                      *
*                   |    '--> .-------------.  *
*                   |        | ref. Fragment | *
*                   |    .--> '-------------'  * 
*  .----------.     |   |                      * 
* | instance 2 | *--T2-'                       *
*  '----------'     |                          *
************************************************


!!! Note
    Fragment usage example
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    -- ----------------------------------------------------------------------------- 
    -- set new fragment
    -- ----------------------------------------------------------------------------- 
    molecule = Fragment:new("super_large_system.cell")
    
    -- -----------------------------------------------------------------------------
    -- defines the basis sets in use for all elements, from basis lib
    -- -----------------------------------------------------------------------------
    molecule:setKohnShamSpecies( "*","cc-pvtz")
    
    -- ----------------------------------------------------------------------------- 
    -- set fragment Kohn Sham orbitals from previous DFT calculation
    -- ----------------------------------------------------------------------------- 
    molecule:setDysonOrbitals("super_large_system.WFN")
    
    -- ----------------------------------------------------------------------------- 
    -- compuyte fragment MP2 correlation energy
    -- ----------------------------------------------------------------------------- 
    Ec = molecule:MP2Energy()
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


# [Instanciation]("Fragment_instanciation.md.html")

Methods that are used to create fragment and handle their spatial transforms. 

method                                                   | description        
---------------------------------------------------------|-----------------------------------------------------
[new]("Fragment_instanciation.md.html#new")              | creates a new reference fragment form atomic coordinates
[transform]("Fragment_instanciation.md.html#transform")  | creates a new instance of an existing reference fragment 
[copy]("Fragment_instanciation.md.html#copy")            | creates an independent copy of a reference fragment


# [Parralel environment]("Fragment_penv.md.html")

Specific handling of a fragment parallel environment 

method                                     | description        
-------------------------------------------|-----------------------------------------------------
[setPEnv]("Fragment_penv.md.html#setpenv") | sets fragment specific parallel environment 


# [Basic quantities]("Fragment_basic_quantities.md.html")

Mostly scalar fragment quantities getter, such as number of electron and such...

method                                                                 | description        
-----------------------------------------------------------------------|-------------------------------
[charge]("Fragment_basic_quantities.md.html#charge")                   | fragment total charge state (ion+el.) setter/getter
[densityMonopole]("Fragment_basic_quantities.md.html#densitymonopole") | electonic charge from fitted density
[nvalence]("Fragment_basic_quantities.md.html#nvalence")               | fragment number of valence states getter
[noccupied]("Fragment_basic_quantities.md.html#noccupied")             | fragment number of occupied states getter
[nvirtual]("Fragment_basic_quantities.md.html#nvirtual")               | fragment number of virtual states getter
[ncore]("Fragment_basic_quantities.md.html#ncore")                 | fragment number of core states getter
[xcGridCharge]("Fragment_basic_quantities.md.html#xcgridcharge")       | electronic charge from integration on exchange correlation grid
[density]("Fragment_basic_quantities.md.html#density")                 | fitted density/codensity on the auxiliary basis

# [Basis sets]("Fragment_basis.md.html")

Fragment various basis set definitions

method                                                              | description        
--------------------------------------------------------------------|-------------------------------
[setKohnShamSpecies]("Fragment_basis.md.html#setkohnshamspecies")   | sets fragment khon sham basis
[setAuxiliarySpecies]("Fragment_basis.md.html#setauxiliaryspecies") | sets fragment auxiliary basis
[setRealspaceSpecies]("Fragment_basis.md.html#setrealspacespecies") | sets fragment real space basis
[setModelSpecies]("Fragment_basis.md.html#setModelSpecies")         | sets fragment model basis
[setConstraintsSpecies]("Fragment_basis.md.html#setConstraintsSpecies") | sets fragment constraints basis

# [Resolution of the identity]("Fragment_RI.md.html")

Resolution of the identity is a central component of beDeft. 
Here are the methods that allow tuning Resolution of the identity type and parameters in use.

method                                                   | description        
---------------------------------------------------------|-----------------------------------------------------
[setStandardRI]("Fragment_RI.md.html#setstandardri")     | sets standard resolution of the identity
[setRealspaceRI]("Fragment_RI.md.html#setrealspaceri")   | sets realspace resolution of the identity
[setLTMode]("Fragment_RI.md.html#setltmode")             | sets laplace transform mode for $\chi_0$ calculation

# [Dyson orbitals (molecular orbitals)]("Fragment_dyson_orbitals.md.html")

Methods related to the fragment orbital set

method                                                   | description        
---------------------------------------------------------|-----------------------------------------------------
[setDysonOrbitals]("Fragment_dyson_orbitals.md.html#setdysonorbitals")         | sets fragment dyson orbitals from precalculated data 
[dumpDysonOrbitals]("Fragment_dyson_orbitals.md.html#dumpdysonorbitals")       | dump fragment dyson orbitals 
[NWChemDysonOrbitals]("Fragment_dyson_orbitals.md.html#nwchemdysonorbitals")   | driver handling calculation of dyson orbitals through NWChem
[OrcaDysonOrbitals]("Fragment_dyson_orbitals.md.html#orcadysonorbitals")       | driver handling calculation of dyson orbitals through Orca
[dysonOrbital]("Fragment_dyson_orbitals.md.html#dysonorbitals")                | returns the requested gaussian orbital as gaussian function object
[dysonOrbitalsEkin]("Fragment_dyson_orbitals.md.html#dysonorbitalsekin")       | returns dyson orbitals kinetic energies
[dysonOrbitalsEion]("Fragment_dyson_orbitals.md.html#dysonorbitalseion")       | returns dyson orbitals ion energy contribution
[dysonOrbitalsEha]("Fragment_dyson_orbitals.md.html#dysonorbitalseha")         | returns dyson orbitals Hartree energy contribution
[dysonOrbitalsExc]("Fragment_dyson_orbitals.md.html#dysonorbitalsexc")         | returns dyson orbitals exchange-correlation energy contribution
[dysonOrbitalsExx]("Fragment_dyson_orbitals.md.html#dysonorbitalsexx")         | returns dyson orbitals exact-exchange energy contribution
[dysonOrbitalsEqp]("Fragment_dyson_orbitals.md.html#dysonorbitalseqp")         | returns dyson orbitals quasiparticle energies
[dysonOrbitalsGap]("Fragment_dyson_orbitals.md.html#dysonorbitalsgap")         | returns dyson orbitals HOMO-LUMO gap
[dysonOrbitalsHOMO]("Fragment_dyson_orbitals.md.html#dysonorbitalshomo")       | returns dyson orbitals HOMO quasiparticle energy
[dysonOrbitalsLUMO]("Fragment_dyson_orbitals.md.html#dysonorbitalslumo")       | returns dyson orbitals LUMO quasiparticle energy
[reorderDysonOrbitals]("Fragment_dyson_orbitals.md.html#reorderDysonOrbitals") | reorder dyson orbitals to match a reference fragment

# [GW calculation]("Fragment_GW.md.html")

method                                              | description        
----------------------------------------------------|-------------------------------
[setGWCorrectedOcc]("Fragment_GW.md.html#setGWCorrectedOcc") | sets the number of occupied states whose Quasi-Particles energies are corrected at GW level
[setGWOccSubspace]("Fragment_GW.md.html#setGWOccSubspace")   | sets an energy window (in Hartree) for occupied states whose Quasi-Particles energies are corrected at GW level
[GWCorrectedOcc]("Fragment_GW.md.html#GWCorrectedOcc")       | getter of the number of occupied states whose Quasi-Particles energies are corrected at GW level 
[setGWCorrectedVir]("Fragment_GW.md.html#setGWCorrectedVir") | sets the number of virtual states whose Quasi-Particles energies are corrected at GW level
[setGWVirSubspace]("Fragment_GW.md.html#setGWVirSubspace")   | sets an energy window (in Hartree) for virtual states whose Quasi-Particles energies are corrected at GW level
[GWCorrectedVir]("Fragment_GW.md.html#GWCorrectedVir")       | getter of the number of virtual states whose Quasi-Particles energies are corrected at GW level 

# Energy calculation

method                                              | description        
----------------------------------------------------|-------------------------------
[MP2Energy]("Fragment_MP2Energy.md.html#mp2energy") | Fragment MP2 correlation energy
[RPAEnergy]("Fragment_RPAEnergy.md.html#rpaenergy") | Fragment RPA correlation energy
[TDHF]("Fragment_TDHF.md.html#tdhf")                | Correlation energy from time dependent calculation 

# [Linear response]("Fragment_linear_response.md.html")    

method                                                                   | description        
-------------------------------------------------------------------------|-------------------------------
[V]("Fragment_linear_response.md.html#v")                                | returns bare coulomb operator within the auxiliary basis
[W]("Fragment_linear_response.md.html#w")                                | returns screened coulomb operator within the auxiliary basis
[Chi0]("Fragment_linear_response.md.html#chi0")                          | returns $\chi_0$ suceptibility operator within the auxiliary basis
[densityResponse]("Fragment_linear_response.md.html#densityresponse")    | fragment density response to an external perturbation
[TwoBodyInteraction]("Fragment_linear_response.md.html#densityresponse") | 2-body interaction acces between codensitites

# [PCM]("Fragment_PCM.md.html")

method                                                       | description        
-------------------------------------------------------------|-------------------------------
[cavityStep]("Fragment_PCM.md.html#cavitystep")              | get/set discretization step (a.u.) for PCM cavity description
[cavityData]("Fragment_PCM.md.html#cavitydata")              | get/set atomic radii data for PCM cavity description
[cavityFactor]("Fragment_PCM.md.html#cavityfactor")          | get/set atomic radius scaling factor for PCM cavity description
[epsOptic]("Fragment_PCM.md.html#epsoptic")                  | get/set optic outer dielectric constant
[epsStatic]("Fragment_PCM.md.html#epsstatic")                | get/set static outer dielectric constant
[solvent]("Fragment_PCM.md.html#solvent")                    | set solvent in use for PCM embedding

# Miscellaneous

method                                                   | description        
---------------------------------------------------------|------------------------------
[FiestaGW]("Fragment_miscellaneous.md.html#fiestagw")    | driver handling fiesta calulation from within beDeft
[GSGreedy]("Fragment_miscellaneous.md.html#gsgreedy")    | ground state multideterminant greedy solver - experimental
[CIDyson]("Fragment_miscellaneous.md.html#cidyson")      | dyson orbitals from CI ground state - experimental
[__mul]("Fragment_miscellaneous.md.html#__mul")          | multiplication by a rigid motion object (spatial transform)
[__tostring]("Fragment_miscellaneous.md.html#cidyson")   | display fonction, essentially print fragment atoms.



<style class="fallback">body{visibility:hidden}</style><script>markdeepOptions={tocStyle:'custom', docParent:'../index.md.html' };</script>
<!-- Markdeep: --><script src="../markdeep.js?" charset="utf-8"></script>



